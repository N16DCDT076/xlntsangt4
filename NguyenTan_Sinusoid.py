
import numpy as np                             # them thu vien numpy de xu ly mang
import matplotlib.pyplot as plt                # them thu vien matplotlib.pyplot de xu ly ve do hoa
A = .8                                         #tao bien do A = 0.8
f = 5                                          # tao tan so f = 5
t = np.arange(0,1,.01)                         #tao ra mang t,bat dau tu 0 ,buoc nhay la 0.1, ket thuc la 1
phi = np.pi/4                                  # tao goc phi = pi/4
x = A*np.cos(2*np.pi*f*t + phi)                #tao mang x theo cong thuc x ( t ) = A c o s ( 2 pft + f )
plt.plot(t,x)                                  # truyen cac doi so dau vao (t,x) theo truc (x,y)
plt.axis([0,1,-1,1])                           # gioi han khung ve, x=0->1, y=-1->1
plt.xlabel('time in seconds')                  #dat nhan cho truc x l� time in seconds
plt.ylabel('amplitude')                        #dat nhan cho truc y la amplitude
plt.show()                                     #goi bieu do duoc yeu cau ra man hinh
A = .65                                        #bien do
fs = 100 				                       #so lan lay mau
samples = 100 				                   #phan tu
f = 5 					                       #tan so
phi = 0 				                       #goc phi
n = np.arange(samples) 		        	       #tao mang 100 phan tu 0->100
T = 1.0/fs # Remember to use 1.0 and not 1!    #thoi gian lay mau
y = A*np.cos(2*np.pi*f*n*T + phi)	           #tao ra mang x theo cong thuc A*np.cos(2*np.pi*f*n*T + phi)
plt.plot(y)				                       #ve y
plt.axis([0,100,-1,1])			               #gioi han khung ve
plt.xlabel('sample index')		               #dat nhan cho truc x
plt.ylabel('amplitude')			               #dat nhan cho truc y
plt.show()				                       #goi bieu do va yeu cau in ra man hinh
A = .8					                       #tao bien do
N = 100 # samples		            	       #tao 100 phan tu de lay mau
f = 5					                       #tao tan so
phi = 0					                       #tao phi =0
n = np.arange(N)			                   #tao 1 mang 100 phan tu 0->100
y = A*np.cos(2*np.pi*f*n/N + phi)              #tao ra mang y theo cong thuc y = A*np.cos(2*np.pi*f*n/N + phi)
plt.plot(y)				                       #truyen y vao truc
plt.axis([0,100,-1,1])			               #gioi han khung ve
plt.xlabel('sample index')		               #dat nhan cho khung x
plt.ylabel('amplitude')			               #dat nhan cho khung y
plt.show()    				                   #goi bieu do va yeu cau in ra man hinh
f = 3 					                       #tao tan so
t = np.arange(0,1,.01) 			               #tao ra mang t,bat dau tu 0 ,buoc nhay la 0.1, ket thuc la 1
phi = 0 				                       #tao phi
x = np.exp(1j*(2*np.pi*f*t + phi)) 	           #tao mang x = x=ej(2pfnT+?)
xim = np.imag(x) 			                   #tao mang xim la mang phan ao cua x
plt.figure(1)				                   #tao khung ve trong
plt.plot(t,np.real(x))			               #truyen cac doi so dau vao la mang t va real(x) theo truc x,y
plt.plot(t,xim)				                   #truyen cac doi so dau vao la mang t va xim theo truc x,y
plt.axis([0,1,-1.1,1.1])		               #gioi han khung ve
plt.xlabel('time in seconds')		           #dat nhan dan cho truc x
plt.ylabel('amplitude')			               #dat nhan dan cho truc y
plt.show()				                       #goi bieu do va yeu cau in ra man hinh
f = 3					                       #tao tan so
N = 100					                       #tao 100 ph?n t?
fs = 100				                       #l?y m?u 100 l?n
n = np.arange(N)			                   #t?o m?ng 100 ph?n t?
T = 1.0/fs				                       #th?i gian l?y m?u
t = N*T					                       #do dai cua truc x
phi = 0					                       #tao phi
x = np.exp(1j*(2*np.pi*f*n*T + phi))	       #tao mang x=ej(2pfnT+?
xim = np.imag(x)			                   #tao mang xim la mang phan aocua x
plt.figure(1)				                   #tao khung ve trong
plt.plot(n*T,np.real(x))		               #truyen doi so n*T(thoi gian cua tung buoc nhay) va real(x) vao truc x,y
plt.plot(n*T,xim)			                   #truyen doi so n*T(thoi gian cua tung buoc nhay) va xim vao truc x,y
plt.axis([0,t,-1.1,1.1])		               #gioi han khung ve
plt.xlabel('t(seconds)')		               #dat nhan dan cho truc x
plt.ylabel('amplitude')			               #dat nhan dan cho truc y
plt.show()				                       #gioi han khung ve
f = 3				                           #tao tan so
N = 64 					                       #tao N = 64
n = np.arange(64)			                   #tao mang co N(64) phan tu
phi = 0					                       #tao phi
x = np.exp(1j*(2*np.pi*f*n/N + phi))	       #tao mang x =ej ( 2 pfn / N+ F ) .
xim = np.imag(x)                               #tao mang phan ao cua x
plt.figure(1)				                   #tao khung ve trong
plt.plot(n,np.real(x))			               #truyen vao doi so n(thu tu) va real(x) vao truc x,y
plt.plot(n,xim)				                   #truyen vao doi so n(thu tu) va xim vao truc x,y
plt.axis([0,samples,-1.1,1.1])	    	       #gioi han khung ve
plt.xlabel('sample index')		               #dat ten cho truc x
plt.ylabel('amplitude')            	           #dat ten cho truc y
plt.show()				                       #goi bieu do va yeu cau in ra man hinh
import scipy.io as scipy		               #them thu vien scipy
N = 44100 # samples			                   #tao N = 44100 phan tu
f = 440					                       #tao tan so
fs = 44100				                       #so lan lay mau
phi = 0					                       #tao phi
n = np.arange(N)		            	       #tao mang co N(44100) phan tu
x = A*np.cos(2*np.pi*f*n/N + phi)	           #tao mang x
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write	           #ghi du lieu vao wavfile
write('sine440_1sec.wav', 44100, x)	           #du lieu gi: 'sine440_1sec.wav', 44100, x(1 gi�y ? 440 Hz)