#problem 1
s = 'Hi John, welcome to python programming for beginner!'
print('trong chuoi s = ',s)
a = s.count('python')
if a > 0 :
    print('co ton tai chuoi python')
else:
    print('khong ton tai chuoi python')
if s.count('John') > 0 :
    s1 = 'john'
    print('s1 bang',s1)
else:
    print('khong ton tai chuoi John')
print('co tong cong',s.count('o'),'chu o')
b = s.split(' ',len(s))
print('co tong cong ',len(b), 'tu ')
#problem2
l = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]
print('mang l = ',l)
l.remove('python')
print('mag sau khi da xoa phan tu python',l)
dem1 = l.count(4.2)
if dem1 > 0 :
    print('mang l co phan tu là 4.2')
else:
    print('mang không có phần tử 4.2')
#problem3
A = {3,4,5,6,7}
B = {2,4,5,9,12,24}
C = {2,4,8}
print('A : ',A)
print('B : ',B)
print('C : ',C)
B = B | C
A = A | C
print('B SAU KHI THEM C ',B)
print('A SAU KHI THEM C ',A)
D = A & B
print('giao diem cua A va B ',D)
E = A | B
print('tap hop A VA B',E)
print('Phan tu A khong co trong B ',A - D)
print('chieu dai cua A ', len(A))
print('chieu dai cua B ', len(B))
print('gia tri lon nhat cua A ',max(A))
print('gia tri lon nhat cua B ',max(B))
print('gia tri nho nhat cua A ',min(A))
print('gia tri nho nhat cua B ',min(B))
#problem 4
tub1 = (1,)
tub2 = ('python',)
tub3 = (2,3)
tub4 = (4,5)
print('tuples 1 la ',tub1)
print('tuples 2 la ',tub2)
print('tuples 3 la ',tub3)
print('tuples 4 la ',tub4)
t = tub1 + tub2 + tub3 +tub4
print('t la ',t)
print('phan tu cuoi cung cua t la ',t[len(t)-1])
t = t + (2,3)
s2 = t.count(2)
s3 = t.count(3)
print('danh sach t sau khi them (2,3) la ',t)
if (s2 > 1) & (s3 >1) :
    print('danh sach 2,3 co trung lap trong t')
else:
    print('danh sach 2,3 khong trung lap trong t')
t1 = list(t)
for i in range(0,2):
    t1.remove(2)
    t1.remove(3)
t = tuple(t1)
print('t sau khi bo phan tu 2,3',t)
t = list(t)
print('mang t la', t)
#problem 5
dic1={1:10, 2:20}
dic2={3:30, 4:40}
dic3={5:50,6:60}
dic4 = {}
for d in (dic1, dic2, dic3): dic4.update(d)
print('Tu dien tong: ', dic4)
string = 'Python is an easy language to learn'
dict = {}
for kytu in string:
    dict[kytu] = dict.get(kytu, 0) + 1
print('Cau c: Output = ',dict)
#problem6
R = 3
C = 4
matrix = []
print("Cau a: Nhap gia tri:")
for i in range(R):
    a = []
    for j in range(C):
        print('nhap matrix[', i, '][', j, ']:')
        a.append(int(input()))
    matrix.append(a)
for i in range(R):
        for j in range(C):
            print(matrix[i][j], end=" ")
import numpy as np
random_matrix_list = np.random.randint(1,10,size=(3,4))
print('Ma tran tu list ngau nhien la: ')
print(random_matrix_list)
#problem7
Z = ['Python', 'Bill', 'Trump', 'Pascal','Java']
str='speaking at the white house, trump said the nation is overcome with shock, horror and sorrow. according to bill gate who said that python nowadays is great programming language for data scientist while java still be the first for mobile apps. pascal was dead. other news, please refer here...'
b=str.capitalize()
k=b.replace('python',Z[0])
l=k.replace('trump',Z[2])
m=l.replace('bill',Z[1])
n=m.replace('java',Z[4])
x=n.replace('pascal',Z[3])
print('New String: ',x)
#problem8
import random
def Rand(start, end, num):
	res = []
	for j in range(num):
		res.append(random.randint(start, end))
	return res
num = 1000
start = 0
end = 50
print('Cau a: Day so ngau nhien la: ',Rand(start, end, num))
print('Cau b: ')
import collections
def count(listOfTuple):
    flag = False
    val = collections.Counter(listOfTuple)
    uniqueList = list(set(listOfTuple))
    for i in uniqueList:
        if val[i] >= 2:
            flag = True
            print(i, "xuat hien", val[i],'lan')
    if flag == False:
        print("Duplicate doesn't exist")
listOfTuple = Rand(start, end, num)
count(listOfTuple)
#Xoa cac phan tu lap
def Remove(duplicate):
	final_list = []
	for num in duplicate:
		if num not in final_list:
			final_list.append(num)
	return final_list
duplicate = listOfTuple
print('List fomat = ',Remove(duplicate))
#Cau C
import random
print('Cau C:')
N=int(input("Nhap N:"))
a=random.randrange(N)
b=random.randrange(N-a)
c=N-a-b
list =[a,b,c]
print('List co tong gia tri bang N la : ',list)
#problem9
import decimal
def float_range(start, stop, step):
  while start < stop:
    yield float(start)
    start += decimal.Decimal(step)

print('Cau a: List: ',list(float_range(0, 5.1, '0.1')))
import math
print('Cau b: f(x) = 2*cos(x)')
x= float(input('       X    = '))
print('       f(x) = ',2*math.cos(x))
#problem10
import numpy as np

A = np.array([[1,2,3], [4,5,6]])
B = np.array([[2, 3], [4, 5], [6, 7]])
C = A.dot(B)
print('Kết quả là C=',C)
#problem11
a = [(1,2,3), (2,5,3), (2,4,6,8), (3,6,4)]
for i in range(len(a)):
    a[i]=list(a[i])
    a[i].sort()
    print(a[i])
    c=a[i]
    c= tuple(c)
    for j in range (len(a[i])):
        a[i][j]=a[i][j]/(c[0]+c[-1])

print(('Kết Quả:'),a)
#problem12
tensanpham = ['ten1', 'ten2', 'ten3', 'ten4','ten5']
sanpham =  ['quần', 'áo', 'nón', 'giày','vớ']
giasanpham = [200,150,100,300,50]
print('Tên sản phẩm gồm:      ',tensanpham)
print('Các loại sản phẩm gồm: ',sanpham)
print('Giá sản phẩm gồm:      ',giasanpham)
print('\n\t\tBẢNG BÁO GIÁ\t\t\n')
for z in range (0, len(sanpham)):
    if (z==0):
        print('Loại sản phẩm 1: ',sanpham[0])
        print('Tên sản phẩm 1: ',tensanpham[0])
        print('Giá sản phẩm 1: ',giasanpham[0])
        print('\n')
    elif (z==1):
        print('Loại sản phẩm 2: ',sanpham[1])
        print('Tên sản phẩm 2: ',tensanpham[1])
        print('Giá sản phẩm 2: ',giasanpham[1])
        print('\n')
    elif (z==2):
        print('Loại sản phẩm 3: ',sanpham[2])
        print('Tên sản phẩm 3: ',tensanpham[2])
        print('Giá sản phẩm 3: ',giasanpham[2])
        print('\n')
    elif (z==3):
        print('Loại sản phẩm 4: ',sanpham[3])
        print('Tên sản phẩm 4: ',tensanpham[3])
        print('Giá sản phẩm 4: ',giasanpham[3])
        print('\n')
    else :
        print('Loại sản phẩm 5: ',sanpham[4])
        print('Tên sản phẩm 5: ',tensanpham[4])
        print('Giá sản phẩm 5: ',giasanpham[4])

print('\n\t\tTổng Giá = ',sum(giasanpham))

sapxep = sorted(giasanpham,reverse = True)
Dic = {
       300 : "giày " 
            "ten4 "
            "300",
       200 : "quần "
            "ten1 "
            "200",
       150 : "áo "
            "ten2 "
            "150",
       100 : "nón "
            "ten3 "
            "100",
        50 : "vớ"
            "ten4"
            "50"
       }
print('\nBa Sản Phẩm Hàng Đầu: ')
print()
for k in range (0,(len(sapxep))-1):
    if (k==0):
        print(("\t\t"),Dic[300])
    elif (k==1):
        print(("\t\t"),Dic[200])
    elif (k==2):
        print(("\t\t"),Dic[150])

